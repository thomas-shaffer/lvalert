LVAlert Users Guide
===================

The LIGO-Virgo Alert System (LVAlert) is a notification 
service built on the XMPP_ protocol and the pubsub extension. 
It provides a basic notification tool which allows multiple producers 
and consumers of notifications. 

The current and legacy (to be depreciated)  LVAlert clients are powered by  
SleekXMPP_ and PyXMPP_, respectively. The server backend is powered by
Openfire_ v4.2.3.

First Steps
-----------

To use the service, you must first visit the `Account Activation`_  webpage 
to set your LVAlert password. Your username is based on your "albert.einstein"
LIGO.org credentials. To obtain a user or robot account  with a unique username, 
please contact `Alexander Pace`_ or `Tanner Prestegard`_. 

LVAlert How To
--------------

LVAlert uses the Publish-Subscribe (PubSub) model to create and distribute 
alerts to users. An entity (most commonly, GraceDB_) publishes an alert
to a node (think of it like a channel). Other entities subscribe to that node 
(channel) and then listen for content published on the channel. 

Alerts from GraceDB take the form of JSON_-formatted strings, whose contents 
depend on the action from GraceDB (e.g.: new event upload, new label applied, 
etc.). A description of LVAlert message contents from GraceDB is available for events_ 
and superevents_.

The listener can be configured to take an action upon receipt of an alert.


.. _XMPP: https://xmpp.org/
.. _SleekXMPP: http://sleekxmpp.com
.. _PyXMPP: http://pyxmpp.jajcus.net/
.. _Openfire: https://www.igniterealtime.org/projects/openfire/
.. _Account Activation: https://www.lsc-group.phys.uwm.edu/cgi-bin/jabber-acct.cgi
.. _Alexander Pace: mailto:alexander.pace@ligo.org
.. _Tanner Prestegard: mailto:tanner.prestegard@ligo.org
.. _GraceDB: https://gracedb.ligo.org/
.. _JSON: https://www.json.org/
.. _events: https://gracedb.ligo.org/documentation/lvalert.html#event-alerts
.. _superevents: https://gracedb.ligo.org/documentation/lvalert.html#superevent-alerts
